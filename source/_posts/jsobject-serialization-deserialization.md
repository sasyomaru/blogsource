---
title: 自定义JS对象的序列化与反序列化过程
comments: true
categories:
  - 技术
tags:
  - 天天向上
  - JS
date: 2015-10-08 16:32:08
galleryImage:
galleryImageSource:
excerpt:
  JSON.stringify和JSON.parse的第二个参数使得我们可以控制序列化和反序列化的过程。
---
先更正一个概念，JSON是一种数据表达的格式（见[RFC-4627](http://www.ietf.org/rfc/rfc4627.txt)），就像我们平常说的xml、csv一样。它本身不是数据，也不依赖于具体编程语言。用.net、Java或者Python都能生成JSON格式的数据。
[JSON对象(JSON Object)](http://www.ecma-international.org/ecma-262/5.1/#sec-15.12)(中文文档见[这里](http://lzw.me/pages/ecmascript/#609))这个术语是有的，它是全局对象众多属性中的一个。它包含两个函数：parse和stringify，用于解析和构造JSON文本。但是JSON对象跟[对象初始化(Object Initialiser)](http://www.ecma-international.org/ecma-262/5.1/#sec-11.1.5)(中文文档见[这里](http://lzw.me/pages/ecmascript/#159))是两码事，后者用于以直接量的方式初始化一个对象，比如
```JavaScript
var a = { myField: 'myValue' };
```

注意不要弄混了。

其实JSON.stringify和JSON.parse的参数都不止一个，根据ECMAScript 5.1规范，两者的声明分别是：
```
JSON.stringify(value [ , replacer [ , space ] ] )
JSON.parse(text [ , reviver ] )
```

这就使得我们可以控制序列化和反序列化的过程。比如下面的代码模拟了一个让函数在序列化并反序列化后仍旧保持不丢失的情况。
```JavaScript
var obj = {
  myField: 'myValue',
  myFunction: function() {
    console.log('function is not able to be serialized');
  }
};
function replacer(key, value) {
  if(typeof value === 'function') {
    return '__function';
  } else {
    return value;
  }
};
var JSONString = JSON.stringify(obj, replacer);
function reviver(key, value) {
  if (value === '__function') {
    return function() {
      console.log('function restored by reviver');
    };
  } else {
    return value;
  }
}
var restoredObj = JSON.parse(JSONString, reviver);
restoredObj.myFunction();
```


值得注意的是，除了函数外，replacer还支持其他几种类型。有兴趣的请翻阅[ECMAScript规范相关部分](http://www.ecma-international.org/ecma-262/5.1/#sec-15.12.3)(中文版见[这里](http://lzw.me/pages/ecmascript/#614))。最有意思的是，你还可以通过定义toJSON函数来自定义序列化的过程。
